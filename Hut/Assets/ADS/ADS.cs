﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;

public class ADS : MonoBehaviour {

    enum Device {Xiaomi,Cube};
    [SerializeField] string idBanner;
    [SerializeField] string idPage;
    [SerializeField] AdPosition banerPosition = AdPosition.Bottom;
    [SerializeField] Device device = Device.Xiaomi;

    readonly string XiaomiId = "386D75061B594B29";
    readonly string CubeId = "3563393C2D8B01D1";

    static string staticIdBaner, staticIdPage;
    static AdPosition adPosition;
    static string deviceName;
    static InterstitialAd page;


    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        if(idBanner != null)
          staticIdBaner = idBanner;
        if (idPage != null)
            staticIdPage = idPage;
        adPosition = banerPosition;

        if (device == Device.Xiaomi)
            deviceName = XiaomiId;
        else
            deviceName = CubeId;
    }

    public static void ShowUnityAds()
    {
        if (Advertisement.IsReady())
            Advertisement.Show();
    }

    public static void ShowAdMobBanner()
    {
        BannerView baner = new BannerView(staticIdBaner,AdSize.Banner,adPosition);// создаем банер
        //AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice(deviceName).Build();// для теста на телефоне
        AdRequest request = new AdRequest.Builder().Build(); // для релиза
        baner.LoadAd(request); // показываем рекламу ,полученную из запроса, в бенере
    }

    public static void ShowAdMobPage()
    {
        page = new InterstitialAd(staticIdPage);
        //AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice(deviceName).Build();// для теста на телефоне
        AdRequest request = new AdRequest.Builder().Build();
        page.LoadAd(request);
        page.OnAdLoaded += OnLoadPage;
    }

    static void OnLoadPage(object sender, System.EventArgs e)
    {
        page.Show();
    }

}
